#!/usr/bin/env bash

# Colors and Font types
# ============================================
NOCOLOR='\033[0m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
ORANGE='\033[0;33m'


# Functions
# ============================================
title() {
    echo -e "${CYAN}$@${NOCOLOR}"
}

success() {
    echo -e "${GREEN}✔${NOCOLOR} $@"
}

info() {
    echo -e "${ORANGE}$@${NOCOLOR}"
}

# 1. Update the apt package index and install packages to allow apt to use a repository over HTTPS:
installPackages() {
	sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common > /dev/null 2>&1
	success "Packages installed."
}

# 2. Add Docker’s official GPG key:
addDockersKey() {
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -  > /dev/null 2>&1
	success "Official Docker key added."
}

# 3. Use the following command to set up the stable repository.
addDockerRepository() {
	sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable" > /dev/null 2>&1
	success "Official Docker repository added."
}

# 4. Update the apt package index, and install the latest version of Docker Engine and containerd
installDocker() {
	sudo apt-get update > /dev/null 2>&1
	sudo apt-get -y install docker-ce docker-ce-cli containerd.io > /dev/null 2>&1
	success "Docker installed."
	version=$(docker -v)
	info ${version}
}


hello() {
	title "Executing install-docker.sh"
}


# Algorithm
# ================================
hello
installPackages
addDockersKey
addDockerRepository
installDocker

