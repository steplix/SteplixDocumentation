#!/usr/bin/env bash

# Colors and Font types
# ============================================
NOCOLOR='\033[0m'
CYAN='\033[0;36m'


# Functions
# ============================================
title() {
    echo -e "${CYAN}$@${NOCOLOR}"
}

installSteplix() {
	wget https://gitlab.com/steplix/SteplixClients/-/raw/master/bash-client/install.sh > /dev/null 2>&1
	chmod +x install.sh
    source install.sh
    rm install.sh
}

hello() {
	title "Executing install-steplix.sh"
}


# Algorithm
# ================================
hello
installSteplix


