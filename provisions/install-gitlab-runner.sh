#!/usr/bin/env bash

# Colors and Font types
# ============================================
NOCOLOR='\033[0m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
ORANGE='\033[0;33m'


# Arguments
# ============================================
ubuntuUser=$1


# Functions
# ============================================
title() {
    echo -e "${CYAN}$@${NOCOLOR}"
}

success() {
    echo -e "${GREEN}✔${NOCOLOR} $@"
}

info() {
    echo -e "${ORANGE}$@${NOCOLOR}"
}

installGitlabRunner() {

	# warn user
	info "I am downloading 50 Megabytes :)"

	# Go!
	curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb > /dev/null 2>&1
	dpkg -i gitlab-runner_amd64.deb > /dev/null 2>&1
	rm gitlab-runner_amd64.deb

	# bye!
	success "Gitlab Runner installed.";
	version=$(gitlab-runner --version 2>&1 | head -n 1 | awk -F ' ' '{print $2}')
	info ${version}
}

makeTest() {

	# hack: .gitconfig to root user
	cp "/home/${ubuntuUser}/.gitconfig" /root/.gitconfig

	# create a test folder
	local foldername="/home/${ubuntuUser}/test-gitlab"
	rm -rf ${foldername}
	mkdir ${foldername}

	# move to folder
	cd ${foldername}

	# initialize git
	git init > /dev/null 2>&1
	git commit --allow-empty -n -m "Initial commit". > /dev/null 2>&1

	# make the .yml file
	local filename="/home/${ubuntuUser}/test-gitlab/.gitlab-ci.yml"

	echo "stages:" >> ${filename}
	echo "  - test" >> ${filename}
	echo "" >> ${filename}
	echo "myTestingTask:" >> ${filename}
	echo "  stage: test" >> ${filename}
	echo "  script:" >> ${filename}
	echo "    - echo \"Hello! What time is it?\"" >> ${filename}
	echo "    - date" >> ${filename}
	echo "    - echo \"Bye!\"" >> ${filename}

	# add as commit because it is required in order to be executable
	git add . > /dev/null 2>&1
	git commit -m "my first gitlab continuos integration .yml test file" > /dev/null 2>&1

	# run!
	success "Well done! You can run test with gitlab: "
	info "$ cd ${foldername}"
	info "$ gitlab-runner exec shell myTestingTask"

	# set permissions
	chown -R "${ubuntuUser}:${ubuntuUser}" ${foldername}

	# hack: clean
	rm /root/.gitconfig
}

hello() {
	title "Executing install-gitlab-runner.sh"
}


# Algorithm
# ================================
hello
installGitlabRunner
makeTest


