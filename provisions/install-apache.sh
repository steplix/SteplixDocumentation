#!/usr/bin/env bash

# Colors and Font types
# ============================================
NOCOLOR='\033[0m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
ORANGE='\033[0;33m'


# Functions
# ============================================
title() {
    echo -e "${CYAN}$@${NOCOLOR}"
}

success() {
    echo -e "${GREEN}✔${NOCOLOR} $@"
}

info() {
    echo -e "${ORANGE}$@${NOCOLOR}"
}

installApache() {
	sudo apt-get update > /dev/null 2>&1
	sudo apt-get install -y apache2 > /dev/null 2>&1
}

bye() {
	success "Apache installed.";
	version=$(apache2 -version 2>&1 | head -n 1)
	info ${version}
}

hello() {
	title "Executing install-apache.sh"
}


# Algorithm
# ================================
hello
installApache
bye


