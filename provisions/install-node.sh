#!/usr/bin/env bash

# Colors and Font types
# ============================================
NOCOLOR='\033[0m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
ORANGE='\033[0;33m'



# Functions
# ============================================
title() {
    echo -e "${CYAN}$@${NOCOLOR}"
}

success() {
    echo -e "${GREEN}✔${NOCOLOR} $@"
}

info() {
    echo -e "${ORANGE}$@${NOCOLOR}"
}

updateRepositories() {
	curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash - >  /dev/null 2>&1
}

installNode() {
	sudo apt-get install -y nodejs >  /dev/null 2>&1
    success "NodeJs installed."
	version=$(node -v)
	info $version
	success "NPM installed."
	version=$(npm -v)
	info $version
}

hello() {
	title "Executing install-node.sh"
}


# Algorithm
# ================================
hello
updateRepositories
installNode
