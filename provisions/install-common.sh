#!/usr/bin/env bash

# Colors and Font types
# ============================================
NOCOLOR='\033[0m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'


# Functions
# ============================================
title() {
    echo -e "${CYAN}$@${NOCOLOR}"
}

success() {
    echo -e "${GREEN}✔${NOCOLOR} $@"
}

installUnzip() {
	sudo apt-get install -y unzip > /dev/null 2>&1
	success "Unzip installed."
}

hello() {
	title "Executing install-common.sh"
}


# Algorithm
# ================================
hello
installUnzip

