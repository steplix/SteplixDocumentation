#!/usr/bin/env bash

# Colors and Font types
# ============================================
NOCOLOR='\033[0m'
CYAN='\033[0;36m'
GREEN='\033[1;32m'
ORANGE='\033[0;33m'

# Arguments
# ============================================
username=$1
rootPassword=$2

# Functions
# ============================================
title() {
    echo -e "${CYAN}$@${NOCOLOR}"
}

success() {
    echo -e "${GREEN}✔${NOCOLOR} $@"
}

info() {
    echo -e "${ORANGE}$@${NOCOLOR}"
}

installMySQL() {
	sudo su
	sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password ${rootPassword}"
	sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password ${rootPassword}"
	sudo apt-get -y -qq install mysql-server > /dev/null 2>&1
    success "MySQL installed."
	version=$(mysql --version)
	info $version
}

createAlias() {
	local filename="/home/${username}/.bash_aliases"
	sudo su
	echo "alias connect-database='mysql --user=root --password=${rootPassword} --host=localhost'" >> ${filename}
	success "Alias created."
	info "$ connect-database"
}

hello() {
	title "Executing install-mysql.sh"
}


# Algorithm
# ================================
hello
installMySQL
createAlias
