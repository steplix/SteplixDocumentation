#!/usr/bin/env bash

# Colors and Font types
# ============================================
NOCOLOR='\033[0m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
ORANGE='\033[0;33m'


# Arguments
# ============================================
ubuntuUser=$1
username=$2
email=$3


# Functions
# ============================================
title() {
    echo -e "${CYAN}$@${NOCOLOR}"
}

success() {
    echo -e "${GREEN}✔${NOCOLOR} $@"
}

info() {
    echo -e "${ORANGE}$@${NOCOLOR}"
}

installGit() {
	sudo apt-get install -y git > /dev/null 2>&1
	success "Git installed.";
	version=$(git --version)
	info ${version}
}

setupUser() {
	git config --global user.name "${username}"
	git config --global user.email "${email}"
    success "Git User created.";
    info ${username}
    info ${email}
}

setupAliases() {
	git config --global alias.co checkout
	git config --global alias.br branch
	git config --global alias.ci commit
	git config --global alias.st status
	success "Git aliases created.";
}

moveFile() {
	local filename="/home/${ubuntuUser}/.gitconfig"
	mv ~/.gitconfig ${filename}
}

hello() {
	title "Executing install-git.sh"
}


# Algorithm
# ================================
hello
installGit
setupUser
setupAliases
moveFile


