#!/usr/bin/env bash

# Colors and Font types
# ============================================
NOCOLOR='\033[0m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
ORANGE='\033[0;33m'

# Arguments
# ============================================
ubuntuUser=$1
accessToken=$2
secretToken=$3
region=$4


# Functions
# ============================================
title() {
    echo -e "${CYAN}$@${NOCOLOR}"
}

success() {
    echo -e "${GREEN}✔${NOCOLOR} $@"
}

info() {
    echo -e "${ORANGE}$@${NOCOLOR}"
}

installAWS() {
	
	# update
	sudo apt-get -y upgrade > /dev/null 2>&1
	sudo apt-get -y update > /dev/null 2>&1

	# install
	sudo apt-get -y install awscli > /dev/null 2>&1
}

setupAWS() {

	local foldername=/home/${ubuntuUser}/.aws
	local configFile="${foldername}/config"
	local credentialsFile="${foldername}/credentials"

	# mkdir
	rm -rf ${foldername}
	mkdir ${foldername}

	# create region file
	echo "[default]" >> ${configFile}
	echo "output = json" >> ${configFile}
	echo "region = ${region}" >> ${configFile}

	# create credentials file
	echo "[default]" >> ${credentialsFile}
	echo "aws_access_key_id = ${accessToken}" >> ${credentialsFile}
	echo "aws_secret_access_key = ${secretToken}" >> ${credentialsFile}

	# permissions
	sudo chown -R ${ubuntuUser}:${ubuntuUser} ${foldername}

	# bye
	success "Amazon Setup completed."
}

bye() {
	# results
	success "Amazon AWS installed."
	version=$(aws --version)
	info ${version}
}

hello() {
	title "Executing install-aws.sh"
}


# Algorithm
# ================================
hello
installAWS
setupAWS
bye


