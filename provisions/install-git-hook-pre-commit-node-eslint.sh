#!/usr/bin/env bash

# 
# Usage:
#    curl -sL wget https://gitlab.com/steplix/SteplixDocumentation/-/raw/master/provisions/install-git-hook-pre-commit-node-eslint.sh?inline=false | sudo -E bash -
#

# Colors and Font types
# ============================================
NOCOLOR='\033[0m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
ORANGE='\033[0;33m'
RED='\e[91m'


# Functions
# ============================================
title() {
    echo -e "${CYAN}$@${NOCOLOR}"
}

success() {
    echo -e "${GREEN}✔${NOCOLOR} $@"
}

info() {
    echo -e "${ORANGE}$@${NOCOLOR}"
}

error() {
    echo -e "${RED}Ooops...${NOCOLOR} $@"
}

check() {
    if [ ! -d .git ]; then
        error "It looks like you are running outside the git repository";
        exit 1
    fi
}

clean() {
    if [ -f "pre-commit" ]; then
        info "Clean directory... Removing previous pre-commit file...";
        rm -rf pre-commit > /dev/null 2>&1
        success "Directory cleaned.";
    fi
}

download() {
    wget https://gist.githubusercontent.com/nicolas-steplix/82ed8a559f7abf026247cee03868cb89/raw/d0375c28b067564027228ce72dda054c08078d77/pre-commit
    if [ ! -f "pre-commit" ]; then
        error "We can't download pre-commit file";
        exit 1
    fi
    success "pre-commit file downloaded.";
}

moveFile() {
    info "Moving downloaded file to .git/hooks folder..."
    mv pre-commit .git/hooks/pre-commit
    chmod 755 .git/hooks/pre-commit
    success "pre-commit is installed correctly.";
}

hello() {
    title "Executing install-git-hook-pre-commit-node-eslint.sh"
}


# Algorithm
# ================================
hello
check
clean
download
moveFile
clean
