#!/usr/bin/env bash

# Colors and Font types
# ============================================
NOCOLOR='\033[0m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
ORANGE='\033[0;33m'


# Functions
# ============================================
title() {
    echo -e "${CYAN}$@${NOCOLOR}"
}

success() {
    echo -e "${GREEN}✔${NOCOLOR} $@"
}

info() {
    echo -e "${ORANGE}$@${NOCOLOR}"
}

installPHP() {
	sudo apt-get -y upgrade > /dev/null 2>&1
	sudo apt-get install -y python-software-properties > /dev/null 2>&1
	sudo add-apt-repository -y ppa:ondrej/php > /dev/null 2>&1
	sudo apt-get -y update > /dev/null 2>&1
	sudo apt-get install -y php7.3 > /dev/null 2>&1
}

installDependencies() {
	sudo apt-get install -y php7.3-common > /dev/null 2>&1
	sudo apt-get install -y php7.3-opcache > /dev/null 2>&1
	sudo apt-get install -y php7.3-cli > /dev/null 2>&1
	sudo apt-get install -y php7.3-gd > /dev/null 2>&1
	sudo apt-get install -y php7.3-curl > /dev/null 2>&1
	sudo apt-get install -y php7.3-mysql > /dev/null 2>&1
	sudo apt-get install -y php7.3-zip > /dev/null 2>&1
	sudo apt-get install -y php7.3-xml > /dev/null 2>&1
	sudo apt-get install -y php7.3-mbstring > /dev/null 2>&1
}

bye() {
	success "PHP installed.";
	version=$(php -v 2>&1 | head -n 1)
	info $version
}

hello() {
	title "Executing install-php7.3.sh"
}


# Algorithm
# ================================
hello
installPHP
installDependencies
bye


