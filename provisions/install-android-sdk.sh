#!/usr/bin/env bash

# Colors and Font types
# ============================================
NOCOLOR='\033[0m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
ORANGE='\033[0;33m'

# Arguments
# ============================================
ubuntuUser=$1


# Functions
# ============================================
title() {
    echo -e "${CYAN}$@${NOCOLOR}"
}

success() {
    echo -e "${GREEN}✔${NOCOLOR} $@"
}

info() {
    echo -e "${ORANGE}$@${NOCOLOR}"
}

installUnzip() {
	sudo apt-get install -y unzip > /dev/null 2>&1
	success "Unzip installed."
}

installAndroidSdk() {

	# warn user
	info "I am downloading 1 Gigabyte :)"
	
	# temp folder
	mkdir androidsdk
	cd androidsdk
	
	# download and unzip
	wget https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip > /dev/null 2>&1
	unzip sdk-tools-linux-3859397.zip > /dev/null 2>&1
	rm sdk-tools-linux-3859397.zip
	
	# move folder to ubuntuUser
	cd ..
	mv androidsdk "/home/${ubuntuUser}"
	chown -R "${ubuntuUser}:${ubuntuUser}" "/home/${ubuntuUser}/androidsdk"

	# add android bash application as part of PATH environment
	local oneline="export PATH=/home/${ubuntuUser}/androidsdk/tools:/home/${ubuntuUser}/androidsdk/tools/bin:$PATH"
	local filename="/home/${ubuntuUser}/.bashrc"
	echo ${oneline} >> ${filename}

	# bye
	success "Android SDK installed."
}


installDependencies() {

	# automatically accept the license
	yes | sudo -u ${ubuntuUser} "/home/${ubuntuUser}/androidsdk/tools/bin/sdkmanager" --licenses > /dev/null 2>&1
	sudo -u ${ubuntuUser} "/home/${ubuntuUser}/androidsdk/tools/bin/sdkmanager" "build-tools;29.0.3" > /dev/null 2>&1
	sudo -u ${ubuntuUser} "/home/${ubuntuUser}/androidsdk/tools/bin/sdkmanager" "platforms;android-29" > /dev/null 2>&1
	success "Android Dependencies installed."
}


hello() {
	title "Executing install-android-sdk.sh"
}


# Algorithm
# ================================
hello
installUnzip
installAndroidSdk
installDependencies


