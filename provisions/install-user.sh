#!/usr/bin/env bash

# Colors and Font types
# ============================================
NOCOLOR='\033[0m'
CYAN='\033[0;36m'
GREEN='\033[1;32m'
YELLOW='\033[0;33m'

# Arguments
# ============================================
username=$1

# Functions
# ============================================
title() {
    echo -e "${CYAN}$@${NOCOLOR}"
}

success() {
    echo -e "${GREEN}✔${NOCOLOR} $@"
}

info() {
    echo -e "${YELLOW}$@${NOCOLOR}"
}

setupLocale() {
	sudo su
    echo "LC_ALL=\"en_US.UTF-8\"" >> /etc/environment
    success "en_US.UTF-8 defined at /etc/environment.";
}

hello() {
	title "Executing install-user.sh"
}

createUser() {
	sudo su
	useradd -m -s /bin/bash -U ${username}
	echo "${username} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
    success "User ${username} created.";
	info "$ sudo su - ${username}"
}

setupUserPrompt() {
	local filename="/home/${username}/.profile"
	sudo su
	echo "" >> ${filename}
	echo "PS1='\[\033[36m\]${username}-local:\033[m\$PWD $ '" >> ${filename}
    success "Prompt style updated in ${filename}.";
}

setupAutoLogin() {
	local filename="/home/vagrant/.profile"
	sudo su
	echo "" >> ${filename}
	echo "sudo su - ${username}" >> ${filename}
	success "Auto Login created in ${filename}.";
}

createKeys() {
	local filename="/home/${username}/.ssh/id_rsa"
	sudo su
	mkdir "/home/${username}/.ssh/"
	ssh-keygen -t rsa -f ${filename} -q -P ""
	chown -R ${username}:${username} "/home/${username}/.ssh/"
	success "Public and Private keys created in ${filename}.";
	local publicKey=`cat ${filename}.pub`
	success "This is your public key:"
	info $publicKey
}


# Algorithm
# ================================
hello
setupLocale
createUser
setupUserPrompt
setupAutoLogin
createKeys
