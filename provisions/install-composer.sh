#!/usr/bin/env bash

# Colors and Font types
# ============================================
NOCOLOR='\033[0m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
ORANGE='\033[0;33m'


# Functions
# ============================================
title() {
    echo -e "${CYAN}$@${NOCOLOR}"
}

success() {
    echo -e "${GREEN}✔${NOCOLOR} $@"
}

info() {
    echo -e "${ORANGE}$@${NOCOLOR}"
}

installComposer() {
	
	# make a folder
	mkdir ~/composer/
	cd ~/composer/

	# download
	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" > /dev/null 2>&1
	php composer-setup.php > /dev/null 2>&1
	php -r "unlink('composer-setup.php');" > /dev/null 2>&1

	# make it globally
	sudo mv composer.phar /usr/local/bin/composer

	# clean
	cd
	rm -rf ~/composer/

	# results
	success "Composer installed."
	version=$(composer --version)
	info ${version}
}

hello() {
	title "Executing install-composer.sh"
}


# Algorithm
# ================================
hello
installComposer


