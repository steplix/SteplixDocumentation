#!/usr/bin/env bash

# Colors and Font types
# ============================================
NOCOLOR='\033[0m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
ORANGE='\033[0;33m'


# Functions
# ============================================
title() {
    echo -e "${CYAN}$@${NOCOLOR}"
}

success() {
    echo -e "${GREEN}✔${NOCOLOR} $@"
}

info() {
    echo -e "${ORANGE}$@${NOCOLOR}"
}

installJava() {
	sudo apt-get install -y openjdk-8-jdk-headless > /dev/null 2>&1
	success "Java installed."
	version=$(java -version 2>&1 | head -n 1 | awk -F '"' '{print $2}')
	info ${version}
}

hello() {
	title "Executing install-java.sh"
}


# Algorithm
# ================================
hello
installJava

