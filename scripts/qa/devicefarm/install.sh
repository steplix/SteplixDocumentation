#!/bin/bash

# helpers
fileExistsOrDie() {

	if ! test -f $1; then
	    echo "$1 does not exist"
	    return 1
	fi

}

# internal variables
current_path=$(pwd)
working_folder="${current_path}/steplix-devicefarm"
common_file="${working_folder}/common.sh"
functions_file="${working_folder}/functions.sh"
variables_file="${working_folder}/variables.sh"
run_file="${working_folder}/run.sh"

# clean previous versions
rm -rf ${working_folder}

# make dir for the new version
mkdir ${working_folder}

# download files
wget -P ${working_folder} https://gitlab.com/steplix/SteplixDocumentation/-/raw/master/scripts/qa/devicefarm/common.sh > /dev/null 2>&1
wget -P ${working_folder} https://gitlab.com/steplix/SteplixDocumentation/-/raw/master/scripts/qa/devicefarm/arguments.sh > /dev/null 2>&1
wget -P ${working_folder} https://gitlab.com/steplix/SteplixDocumentation/-/raw/master/scripts/qa/devicefarm/functions.sh > /dev/null 2>&1
wget -P ${working_folder} https://gitlab.com/steplix/SteplixDocumentation/-/raw/master/scripts/qa/devicefarm/variables.sh > /dev/null 2>&1
wget -P ${working_folder} https://gitlab.com/steplix/SteplixDocumentation/-/raw/master/scripts/qa/devicefarm/run.sh > /dev/null 2>&1

# check file exists
fileExistsOrDie ${common_file}
fileExistsOrDie ${functions_file}
fileExistsOrDie ${variables_file}
fileExistsOrDie ${run_file}

# chmod
chmod 777 ${run_file}