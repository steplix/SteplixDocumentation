#!/bin/bash

help() {

	newline
	title "This script requires the following arguments"
	info "                                    The Project ARN: " "-p arn:aws:devicefarm:us-west-2:275809439666:project:f2f338ba-794a-44ff-8ad0-175165a84b7e"
	info "                                 The DevicePool ARN: " "-d arn:aws:devicefarm:us-west-2::devicepool:082d10e5-d7d7-48a5-ba5c-b33d66efa1f5"
	info "           The filepath for android application APK: " "-a /home/qa/src/app-bancoColumbiaStaging-debug.apk"
	info "             The filepath for instrumented test APK: " "-t /home/qa/src/app-bancoColumbiaStaging-debug-androidTest.apk"
	info "   The path inside s3://steplix-applications bucket: " "-b /banco-columbia/project-witta/qa/"
	info "   The email where the script will send the results: " "-e team-columbia@steplix.com"
	info "Max minutes waiting devicefarm to complete the task: " "-m 10"

}

helpWithError() {
	help
	newline
	error $1
	bye
}

# Ensures that the number of passed args are at least equals
# to the declared number of mandatory args.
checkRequiredArguments() {

	local required_arguments=14	# key + value, key + value, etc.

	if [ ! $1 -eq $required_arguments ]
	then
		newline
		helpWithError "Missing arguments."
		bye
	fi
}

notEmptyOrDie() {

	if [ -z $1 ]
	then
		newline
		helpWithError "Some arguments are empty."
		bye
	fi
}

checkEmptyArguments() {

	notEmptyOrDie ${android_local}
	notEmptyOrDie ${tests_local}
	notEmptyOrDie ${arn_project}
	notEmptyOrDie ${arn_devicepool}
	notEmptyOrDie ${bucket_path}
	notEmptyOrDie ${email}
	notEmptyOrDie ${max_minutes_device_farm}
}


# Assign each argument to each script variables
while getopts ":a:t:p:d:b:e:m:" opt; do
	case $opt in
		a) 		android_local=$OPTARG ;;
		t) 		tests_local=$OPTARG ;;
		p) 		arn_project=$OPTARG ;;
		d) 		arn_devicepool=$OPTARG ;;
		b) 		bucket_path=$OPTARG ;;
		e) 		email=$OPTARG ;;
		m) 		max_minutes_device_farm=$OPTARG ;;
		\?)		helpWithError "Invalid option: -$OPTARG" >&2 ;;
		:)		helpWithError "Option -$OPTARG requires an argument." >&2 ;;
	esac
done
