#!/bin/bash

NOCOLOR='\033[0m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
ORANGE='\033[0;33m'
RED='\033[0;41m'
LIGHT_CYAN='\033[0;96m'

title() {
    echo -e "[$(date +'%Y-%m-%d %H:%M:%S')] ${LIGHT_CYAN}$@${NOCOLOR}"
}

success() {
    echo -e "[$(date +'%Y-%m-%d %H:%M:%S')] ${GREEN}✔${NOCOLOR} $@"
}

info() {
    echo -e "[$(date +'%Y-%m-%d %H:%M:%S')] $1${ORANGE} $2${NOCOLOR}"
}

error() {
    echo -e "[$(date +'%Y-%m-%d %H:%M:%S')] ${RED}✖ $@${NOCOLOR}"
}

bulletpoint() {
	echo -e "[$(date +'%Y-%m-%d %H:%M:%S')] ‣ ${CYAN}$@${NOCOLOR}"
}

newline() {
	echo ""
}

bye() {
	newline
	echo "Script terminated."
	exit $1
}