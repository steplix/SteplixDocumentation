#!/bin/bash


# Install Apps function
# ======================================================================

installJsonParse() {
	info "Installing" "jq (jsonParser)"
	apt-get -y install jq > /dev/null 2>&1
	applicationExistsOrDie "jq"
	version=$(jq --version)
	success $version
}

installAmazonAWS() {
	info "Installing" "aws (Amazon AWS cli)"
	apt-get -y install awscli > /dev/null 2>&1
	applicationExistsOrDie "aws"
	version=$(aws --version)
	success $version
}


# Welcome function
# ======================================================================
welcome() {

	newline
	title "Welcome to Amazon DeviceFarm Script"
	bulletpoint "This script will upload binaries to Amazon DeviceFarm"
	bulletpoint "This script will upload binaries to Amazon S3"
	bulletpoint "This script will create and run a new task in Amazon DeviceFarm for given tests in the selected DevicePool"
	bulletpoint "This script will notify every stakeholder by email"
	bulletpoint "This script will notify every stakeholder by slack"
	newline
	title "Configuration: "
	newline
	info "   android_apk:" "${android_amazon}"
	info "     tests_apk:" "${tests_amazon}"
	info " android_local:" "${android_local}"
	info "   tests_local:" "${tests_local}"
	info "    android_s3:" "${android_s3}"
	info "      tests_s3:" "${tests_s3}"
	info "   arn_project:" "${arn_project}"
	info "arn_devicepool:" "${arn_devicepool}"
	info "      max_mins:" "${max_minutes_device_farm}"
	info "         email:" "${email}"
	newline
	title "Go!"
	newline
}

# Verification or Die functions
# ======================================================================
fileExistsOrDie() {

	if ! test -f "$1"; then
		newline
	    error "File '$1' does not exists."
	    bye 1
	fi

}

applicationExistsOrDie() {

	if ! [ -x "$(command -v $1)" ]; then
	  newline
	  error "Error: $1 is not installed."
	  bye 1
	fi

}

# Upload to AWS Functions
# ======================================================================
uploadToAmazonAWS() {

	# my vars
	local id_project=$1
	local filename=$2
	local filetype=$3
	local localfile=$4
	local fileS3=$5

	# hello!
	info "Uploading" "${localfile}"

	# createSlot
	local json=$(aws devicefarm create-upload --project-arn ${id_project} --name ${filename} --type ${filetype})
	local status=$(echo ${json} | jq --raw-output ".upload.status")

	if [[ ! $status == *"INITIALIZED"* ]]; then
		newline
		error "Cannot aws devicefarm create-upload"
		bye 1
	fi

	local arn=$(echo ${json} | jq --raw-output ".upload.arn")
	local url=$(echo ${json} | jq --raw-output ".upload.url")

	# upload the binary
	uploadBinaryToDeviceFarm ${localfile} ${url} ${arn}
	success ${arn}

	uploadBinaryToS3 ${localfile} ${fileS3}
	success ${fileS3}

	# bye
	function_result=${arn}
}


uploadBinaryToDeviceFarm() {

	# my vars
	local localfile=$1
	local url=$2
	local arn=$3

	# go go go!
	curl -T ${localfile} ${url} > /dev/null 2>&1

	# wait...
	sleep 3

	# check
	local json=$(aws devicefarm get-upload --arn ${arn})
	local status=$(echo ${json} | jq --raw-output ".upload.status")

	if [[ ! $status == *"SUCCEEDED"* ]]; then
		newline
		error "Cannot aws devicefarm get-upload"
		bye 1
	fi
}


uploadBinaryToS3() {

	# my vars
	local localfile=$1
	local destinationfile=$2

	# go go go!
	aws s3 cp ${localfile} ${destinationfile} > /dev/null 2>&1

	# check bash command result
	if [ ! $? -eq 0 ]; then
		newline
		error "Cannot aws s3 cp ${localfile} ${destinationfile}"
		bye 1
	fi

}


# Run DeviceFarm
# ======================================================================
runDeviceFarm() {

	# my vars
	local id_project=$1
	local id_android=$2
	local id_tests=$3
	local id_devicepool=$4
	local id_projectname=$5

	# hello!
	info "Creating a DeviceFarm Task" "${id_projectname}"

	# go go go!
	local json=$(aws devicefarm schedule-run --project-arn ${id_project} --app-arn ${id_android} --device-pool-arn ${id_devicepool} --name ${id_projectname} --test type=INSTRUMENTATION,testPackageArn=${id_tests})
	local status=$(echo ${json} | jq --raw-output ".run.status")

	if [[ ! $status == *"SCHEDULING"* ]]; then
		newline
		error "Cannot aws devicefarm schedule-run"
		bye 1
	fi

	# bye!
	local arn=$(echo ${json} | jq --raw-output ".run.arn")
	success ${arn}

	function_result=${arn}
}



checkResultsInTimeWindow() {

	# my cars
	local arn=$1
	local max_mins=$2

		# hello!
	info "Waiting for DeviceFarm Task completion" "${max_mins} mins"

	# loop through $max_mins
	minutes=1
	while [ $minutes -le $max_mins ]
	do
		# check status
		local json=$(aws devicefarm get-run --arn ${arn})
		local status=$(echo ${json} | jq --raw-output ".run.status")
		local result=$(echo ${json} | jq --raw-output ".run.result")

		if [[ $status == *"COMPLETED"* ]]; then
			success "DeviceFarm task done with the following result: ${result}"
			return 0
		fi
		
		# increment mins
		((minutes++))

		# sleep 1 minute
		sleep 1m

		# progress
		bulletpoint "A minute has passed"
	done

	# bye with error
	newline
	error "More than ${max_mins} mins has passed"
	bye 1
}


# Notifications
# ======================================================================

sendEmail() {

	# my vars
	local endpoint="http://notifications.steplix.com:8000/notifications"
	local arn=$1
	local email=$2
	local android_filename=$3
	local tests_filename=$4
	local android_url=$5
	local tests_url=$6

	# hello
	info "Sending results by email to" "${email}"


	# go!
	local json=$(aws devicefarm get-run --arn ${arn})

	# data variables
	local project_name=$(echo $json | jq --raw-output ".run.name")
	local result=$(echo $json | jq --raw-output ".run.result")
	local devices=$(echo $json | jq --raw-output ".run.totalJobs")
	local tests=$(echo $json | jq --raw-output ".run.counters.total")
	local failed=$(echo $json | jq --raw-output ".run.counters.failed")
	local passed=$(echo $json | jq --raw-output ".run.counters.passed")
	local created=$(echo $json | jq --raw-output ".run.created")
	local stopped=$(echo $json | jq --raw-output ".run.stopped")
	local mins=$(echo $created $stopped | awk '{print ($2 - $1) / 60}')


	# body
	data=$( jq -n \
	      --arg project_name "$project_name" \
	      --arg result "$result" \
	      --arg devices "$devices" \
	      --arg tests "$tests" \
	      --arg failed "$failed" \
	      --arg passed "$passed" \
	      --arg mins "$mins" \
	      --arg android_file "${android_url}" \
	      --arg tests_file "${tests_url}" \
	      --arg android_name "${android_filename}" \
	      --arg tests_name "${tests_filename}" \
	      --arg email "${email}" \
	      --arg subject "[Steplix] [Amazon DeviceFarm] - Result ${project_name}" \
	      '{idTemplate: 1, idChannel: 2, metadata: {project_name: $project_name, result: $result, devices: $devices, tests: $tests, failed: $failed, passed: $passed, mins: $mins, android_file: $android_file, tests_file: $tests_file, android_name: $android_name, tests_name: $tests_name, subject: $subject, to: $email}}' )

	# send
	result=$(curl --silent --header "Content-Type: application/json" --request POST --data "${data}" ${endpoint})

	# check command result
	if [[ ! $result == *"messageId"* ]]; then
		newline
		error "Cannot curl --header \"Content-Type: application/json\" --request POST --data [json] ${endpoint}"
		bye 1
	fi

	# bye!
	success "Mail sent."
	return 0
}


