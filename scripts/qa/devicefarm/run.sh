#!/bin/bash

# Load external modules
source "common.sh"
source "variables.sh"
source "arguments.sh"
source "functions.sh"


# Go go go! El script!
# ======================================================================

checkRequiredArguments $#
checkEmptyArguments

setupVariables

welcome

fileExistsOrDie ${android_local}
fileExistsOrDie ${tests_local}

installJsonParse
installAmazonAWS

uploadToAmazonAWS ${arn_project} ${android_amazon} ${android_type} ${android_local} ${android_s3}
arn_android=${function_result}

uploadToAmazonAWS ${arn_project} ${tests_amazon} ${tests_type} ${tests_local} ${tests_s3}
arn_tests=${function_result}

runDeviceFarm ${arn_project} ${arn_android} ${arn_tests} ${arn_devicepool} ${project_name}
arn_results=${function_result}

checkResultsInTimeWindow ${arn_results} ${max_minutes_device_farm}

sendEmail ${arn_results} ${email} ${android_amazon} ${tests_amazon} ${android_http} ${tests_http}
