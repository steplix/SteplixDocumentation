#!/bin/bash

# Variable para retornar valores de salida de funciones que no sean int
# ======================================================================
function_result=""


# Las siguientes variables deben ser recibidas via argumentos
# ======================================================================
arn_project=""
arn_devicepool=""
max_minutes_device_farm=""
bucket_path=""
android_local=""
tests_local=""
email=""


# estas variables las tenemos que averiguar a medida que sucede el script
# ======================================================================
arn_android="0"
arn_tests="0"
arn_results="0"


# Internal Variables
# ======================================================================
yyyymmddhhiiss=$(date '+%Y%m%d%H%M%S')
project_name="CI-${yyyymmddhhiiss}"

android_amazon="${yyyymmddhhiiss}-android.apk"
tests_amazon="${yyyymmddhhiiss}-tests.apk"
android_type="ANDROID_APP"
tests_type="INSTRUMENTATION_TEST_PACKAGE"


# Internal Variables which dependencies with the arguments
# ======================================================================
bucket=""
bucket_http=""
android_s3=""
tests_s3=""
android_http=""
tests_http=""


# Setup
# ======================================================================

setupVariables() {

	bucket="s3://steplix-applications${bucket_path}"
	bucket_http="https://steplix-applications.s3-sa-east-1.amazonaws.com${bucket_path}"

	android_s3="${bucket}${android_amazon}"
	tests_s3="${bucket}${tests_amazon}"

	android_http="${bucket_http}${android_amazon}"
	tests_http="${bucket_http}${tests_amazon}"
}




